##############################################################################
# Project: Inspira - MRI Wide Bore
# Date: 7/18/14
# Author: Sam Bussmann
# Description: Apply mailability flag and do final selection and output
# Notes: 
##############################################################################

## Get addresses that don't have mailability score of 1

tog_temp<-merge(tog_4,pm[!duplicated(pm$familyid.x),
                         c("mailabilityscorebyte1","addresstype","familyid.x")],by.x="familyid",
                by.y="familyid.x",all.x=T)
Table(tog_temp$mailabilityscorebyte1)
Table(tog_temp$addresstype)


tog_mailable<-tog_temp[tog_temp$mailabilityscorebyte1==1 & tog_temp$addresstype==1,]
table(tog_mailable$mailabilityscorebyte1,tog_mailable$addresstype)

### Remove addresses alredy treated
#tempid<-unique(cmty_wtgt$AddressID_orig[cmty_wtgt$pt_flag==1])

tog_notpts<-tog_mailable#[!(tog_mailable$AddressID %in% tempid),]

### Check 95% quantiles for this given response rate of p
#p<-sum(pm$pt_flag[fac2num(outp) %in% 1:3])/length(pm$pt_flag[fac2num(outp) %in% 1:3])
p<-.03
ctrln<-2000
total<-(6000+ctrln)

#p=.05
##Minus
p-(qhyper(.025,total*p,total*(1-p),ctrln)/ctrln)
##Plus
(qhyper(.975,total*p,total*(1-p),ctrln)/ctrln)-p

##shows equivalency below
# total*p-qhyper(.025,total*p,total*(1-p),ctrln)
# qhyper(.975,total*p,total*(1-p),total-ctrln)

## Select group for mailing and add mail flag

tog_4a<-tog_notpts[order(tog_notpts$score_upd,decreasing=T),]
tog_4b<-tog_4a[1:total,]
tog_4b$mailflag <-1
tog_4b$mailflag[sample.int(total,ctrln)]<-0

table(tog_4b$mailflag,useNA="ifany")

summary(tog_4$score_upd)
summary(tog_4b$score_upd)
mean(tog_4b$score_upd[tog_4b$mailflag==0])
mean(tog_4b$score_upd[tog_4b$mailflag==1])

save(tog_4b,file="Final_Mail_Select.RData")

### Pull and Merge in the address info
sql1<-odbcConnect("IrmSqls")

system.time(
  addr<-sqlQuery(sql1,
                 paste0("SELECT 
                        mp.familyid, mp.individualid, 
                        mp.firstname, mp.middleinitial, mp.lastname, mp.lastnamesuffix,
                        
                        nha.housenumber, nha.streetpredirectional, nha.streetname, 
                        nha.streetsuffix,
                        nha.streetpostdirectional, nha.unittype, nha.unitnumber, nha.city, 
                        nha.state,
                        nha.zipcode,
                        nhin.findincome, mp.age,  mp.gender 
                        FROM ",tenant[tenant$TenantID==96,"DatabaseName"],".[Community].[MarketPerson] mp 
                        inner join ",tenant[tenant$TenantID==96,"DatabaseName"],
                                      ".[Community].[NHAddress] nha on mp.familyid=nha.familyid
                        inner join ",tenant[tenant$TenantID==96,"DatabaseName"],
                        ".[Community].[NHIncome] nhin on mp.familyid=nhin.familyid
                        ")
                               ,errors=T,stringsAsFactors = F,as.is=16)
)


final<-merge(addr[,c("individualid","familyid","firstname", "middleinitial", "lastname", "lastnamesuffix",
                     "housenumber","streetpredirectional","streetname",
                     "streetsuffix","streetpostdirectional",
                     "unittype","unitnumber","city","state","zipcode",
                     "findincome","age","gender")],
             tog_4b[,c("individualid","mailflag")], by="individualid",all.y=T)

dim(final)
Table(final$mailflag)
sum(final$housenumber==" ")

write.csv(final,file="Claxton_Lung_Cancer_Screening_3_5_2015.csv",quote=F,row.names=F)

