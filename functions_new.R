##############################################################################
# Project: Raritan Bay Endocrinology model
# Date: 1/28/2015
# Author: Sam Bussmann
# Description: Create functions to simplify modeling/analysis
# Notes: New Version after 2.0 DB conversion
##############################################################################

library(pryr)
library(glmnet)
library(gtools)

### Create Function to Visualize Categorical data 

viz<-function(varname="OwnRent",df=pm,flag="pt_flag",size=.65){
  par(mfrow=c(2,1))
  t_1<-table(df[,varname],useNA="ifany")
  t<-t_1[mixedorder(names(t_1))]
  t2<-by(df[,flag], df[,varname], FUN=mean,na.rm=T)
  t2<-t2[mixedorder(names(t2))]
  ##Find Number of patients who get NA
  if (sum(is.na(df[,varname]))>0) {
    numptNA<-sum(df[is.na(df[,varname]),flag])
    t2[(length(t2)+1)]<-as.numeric(numptNA/t[length(t)])
    names(t2)[(length(t2))]<-"NA"
  }
  
  one<-barplot(t2,names.arg=names(t2),main=paste("Rates by Group of",varname),cex.names=size)
  text(one, 0, round(t2,5),cex=size,pos=3)
  
  two<-barplot(t,names.arg=names(t2), main=paste("Totals by Group of",varname),cex.names=size)
  text(two, 0, t, cex=size,pos=3) 
}

### Another visualization function for multi group factors (use with df=pm)

viz3<-function(Varname="MaritalStatus",df=pm,flag="group") {
  mosaicplot(factor(df[,flag])~factor(df[,Varname]),main=paste("Mosaic Plot for",Varname),
             xlab="PayerType", ylab=Varname)
}

### For two group vizualization

viz4<-function(varname="OwnRent",df=pm,flag1="pt_drg",flag2="pt_icd9",size=.85){
  par(mfrow=c(3,1))
  
  t<-table(df[,varname],useNA="ifany")[mixedorder(names(table(df[,varname],useNA="ifany")))]
  cond<-(sum(is.na(df[,varname]))>0)
  
  t2<-by(df[,flag1], df[,varname], FUN=mean,na.rm=T)
  t2<-t2[mixedorder(names(t2))]
  ##Find Number of patients who get NA
  if (cond) {
    numptNA<-sum(df[is.na(df[,varname]),flag1])
    t2[length(t2)+1]<-as.numeric(numptNA/t[length(t)])
    names(t2[length(t2)])<-"NA"
  }
  
  t3<-by(df[,flag2], df[,varname], FUN=mean,na.rm=T)
  t3<-t3[mixedorder(names(t3))]
  ##Find Number of patients who get NA
  if (cond) {
    numptNA<-sum(df[is.na(df[,varname]),flag2])
    t3[length(t3)+1]<-as.numeric(numptNA/t[length(t)])
    names(t3[length(t3)])<-"NA"
  }
  
  one<-barplot(t2,names.arg=names(t2),main=paste("Rates of",flag1,"by Group of",varname),cex.names=size)
  text(one, 0, round(t2,6),cex=size,pos=3)
  
  three<-barplot(t3,names.arg=names(t2),main=paste("Rates of",flag2,"by Group of",varname),cex.names=size)
  text(three, 0, round(t3,5),cex=size,pos=3)
  
  two<-barplot(t,names.arg=names(t2), main=paste("Totals by Group of",varname),cex.names=size)
  text(two, 0, t, cex=size,pos=3) 
}



### Create a function to bin, rename and cbind var into new dataset
### Iteratively make sure each bin has at least minN values
### Do so by combining it with its smallest neighbor
## Deals with NA so long as NA is the first value in the table!
## Deals with Max/Min being smallest using conditional processing


binmin<-function(varname1="homevalue", minN=5000, df=cmty_wtgt,flag="pt_flag"){
  library(gtools)
  tempv<-NULL
  tempv<-df[,varname1]
  t<-table(tempv,useNA="ifany")
  nt<-nto<-length(t)
  
  ### Remove leading and trailing whitespace
  trim <- function (x) gsub("^\\s+|\\s+$", "", x)
  
  ### NA Flag - Are there NAs?
  na.test<-FALSE
  na.test<-(sum(is.na(tempv))>0)
  
  ### Blank Flag - Are there Blanks, i.e. " "'s?
  blank.test<-FALSE
  if (is.character(tempv)) blank.test<-(sum((tempv==" BLANK"),na.rm=T)>0)
  
  ### Set the initial condition for the loop
  if (na.test & blank.test) {smallest<-min(t[c(-1,-nt)])} else 
    if (na.test & !blank.test) {smallest<-min(t[-nt])} else
      if (!na.test & blank.test)  {smallest<-min(t[-1])} else { smallest<-min(t)}
  
  ## Initialize counter to ensure no infinite loops
  counter<-0
  while (smallest<minN & counter<=nto){
    
    if (na.test & blank.test) {w<-as.numeric(which(t[c(-1,-nt)]==smallest)[1])+1} else 
      if (na.test & !blank.test) {w<-as.numeric(which(t[-nt]==smallest)[1])} else
        if (!na.test & blank.test)  {w<-as.numeric(which(t[-1]==smallest)[1])+1} else {
          w<-as.numeric(which(t==smallest)[1])}
    
    if (w==1 & !(blank.test) | w==2 & (blank.test)){
      tempv[tempv==names(t[w])]<-tempv[tempv==names(t[w+1])]<-paste(trim(substr(names(t[w]),1,2)),  
                                                                    "-",trim(substr(names(t[w+1]),nchar(names(t[w+1]))-1,nchar(names(t[w+1])))))
    } else 
      if (w==nt & !(na.test) | w==(nt-1) & (na.test)){
        tempv[tempv==names(t[w])]<-tempv[tempv==names(t[w-1])]<-paste(trim(substr(names(t[w-1]),1,2)),
                                                                      "-",trim(substr(names(t[w]),nchar(names(t[w]))-1,nchar(names(t[w])))))
      } else {
        if (t[w+1]<t[w-1]) {tempv[tempv==names(t[w])]<-tempv[tempv==names(t[w+1])]<-paste(trim(substr(names(t[w]),1,2)),
                                                                                          "-",trim(substr(names(t[w+1]),nchar(names(t[w+1]))-1,nchar(names(t[w+1])))))
        } else {
          tempv[tempv==names(t[w])]<-tempv[tempv==names(t[w-1])]<-paste(trim(substr(names(t[w-1]),1,2)),
                                                                        "-",trim(substr(names(t[w]),nchar(names(t[w]))-1,nchar(names(t[w])))))
        }
      }
    t<-table(tempv,useNA="ifany")[mixedorder(names(table(tempv,useNA="ifany")))]
    nt<-length(t)
    
    if (na.test & blank.test) {smallest<-min(t[c(-1,-nt)])} else 
      if (na.test & !blank.test) {smallest<-min(t[-nt])} else
        if (!na.test & blank.test)  {smallest<-min(t[-1])} else { smallest<-min(t)}
    
    counter<-counter+1
    if (counter==nto) print("Infinite Loop!")
  }
  ### output results
  tempout<-data.frame(tempv,df[,flag])
  names(tempout)<-c(varname1,flag)
  return(tempout)
}



binmin2<-function(varname1="homevalue", minN=5000, df=cmty_wtgt,flag1="pt_drg",flag2="pt_icd9"){
  library(gtools)
  tempv<-NULL
  tempv<-df[,varname1]
  t<-table(tempv,useNA="ifany")
  nt<-nto<-length(t)
  
  ### Remove leading and trailing whitespace
  trim <- function (x) gsub("^\\s+|\\s+$", "", x)
  
  ### NA Flag - Are there NAs?
  na.test<-FALSE
  na.test<-(sum(is.na(tempv))>0)
  
  ### Blank Flag - Are there Blanks, i.e. " "'s?
  blank.test<-FALSE
  if (is.character(tempv)) blank.test<-(sum((tempv==" BLANK"),na.rm=T)>0)
  
  ### Set the initial condition for the loop
  if (na.test & blank.test) {smallest<-min(t[c(-1,-nt)])} else 
    if (na.test & !blank.test) {smallest<-min(t[-nt])} else
      if (!na.test & blank.test)  {smallest<-min(t[-1])} else { smallest<-min(t)}
  
  ## Initialize counter to ensure no infinite loops
  counter<-0
  while (smallest<minN & counter<=nto){
    
    if (na.test & blank.test) {w<-as.numeric(which(t[c(-1,-nt)]==smallest)[1])+1} else 
      if (na.test & !blank.test) {w<-as.numeric(which(t[-nt]==smallest)[1])} else
        if (!na.test & blank.test)  {w<-as.numeric(which(t[-1]==smallest)[1])+1} else {
          w<-as.numeric(which(t==smallest)[1])}
    
    if (w==1 & !(blank.test) | w==2 & (blank.test)){
      tempv[tempv==names(t[w])]<-tempv[tempv==names(t[w+1])]<-paste(trim(substr(names(t[w]),1,2)),  
                                                                    "-",trim(substr(names(t[w+1]),nchar(names(t[w+1]))-1,nchar(names(t[w+1])))))
    } else 
      if (w==nt & !(na.test) | w==(nt-1) & (na.test)){
        tempv[tempv==names(t[w])]<-tempv[tempv==names(t[w-1])]<-paste(trim(substr(names(t[w-1]),1,2)),
                                                                      "-",trim(substr(names(t[w]),nchar(names(t[w]))-1,nchar(names(t[w])))))
      } else {
        if (t[w+1]<t[w-1]) {tempv[tempv==names(t[w])]<-tempv[tempv==names(t[w+1])]<-paste(trim(substr(names(t[w]),1,2)),
                                                                                          "-",trim(substr(names(t[w+1]),nchar(names(t[w+1]))-1,nchar(names(t[w+1])))))
        } else {
          tempv[tempv==names(t[w])]<-tempv[tempv==names(t[w-1])]<-paste(trim(substr(names(t[w-1]),1,2)),
                                                                        "-",trim(substr(names(t[w]),nchar(names(t[w]))-1,nchar(names(t[w])))))
        }
      }
    t<-table(tempv,useNA="ifany")[mixedorder(names(table(tempv,useNA="ifany")))]
    nt<-length(t)
    
    if (na.test & blank.test) {smallest<-min(t[c(-1,-nt)])} else 
      if (na.test & !blank.test) {smallest<-min(t[-nt])} else
        if (!na.test & blank.test)  {smallest<-min(t[-1])} else { smallest<-min(t)}
    
    counter<-counter+1
    if (counter==nto) print("Infinite Loop!")
  }
  
  ### output results
  tempout<-data.frame(tempv,df[,flag1],df[,flag2])
  names(tempout)<-c(varname1,flag1,flag2)
  return(tempout)
}

### Easy Table Function

etab<-function(var="creditcard_travel_and_entertainment",df=pm) table(df[,var],useNA="ifany")
etab5<-function(var="creditcard_travel_and_entertainment",df=pm) table(df[,"group"],df[,var],useNA="ifany")

### Add var to modeling dataset called cm

add<-function(var="creditcard_bank",df=pm,bin=F,std=F,cdl=F,dfo=pm){
  ### Write out a format as well as write out the formatted var
  if (bin & !std & !cdl) {
    
    df[,var]<-as.character(df[,var])
    df[is.na(df[,var]),var]<-" MISSING"
    cm[,paste(var,".bin",sep="")]<<-as.factor(df[,var]) 
    
    templ<-cbind(dfo[,var],df[,var])
    lookup<-as.data.frame(templ[!duplicated(templ),])
    dimnames(lookup)[[2]]<-c("start","finish")
    dir.create(file.path(getwd(), "formats"), showWarnings = FALSE)
    write.table(lookup,file=file.path(file.path(getwd(), "formats"),var),row.names=F)
    
  } else if (!bin & !std & !cdl){
    
    df[,var]<-as.character(df[,var])
    df[is.na(df[,var]),var]<-" MISSING"
    cm[,var]<<-as.factor(df[,var])
    
    tempe<-dfo[,var]
    exists<-data.frame(values=tempe[!duplicated(tempe)])
    dir.create(file.path(getwd(), "filters"), showWarnings = FALSE)
    write.table(exists,file=file.path(file.path(getwd(), "filters"),var),row.names=F)
    
  } else if (!bin & std & !cdl){
    
    cm[,paste(var,".std",sep="")]<<-(dfo[,var]-mean(dfo[,var]))/sd(dfo[,var])
    
    temps<-data.frame(mean=mean(dfo[,var]),stdev=sd(dfo[,var]))
    dir.create(file.path(getwd(), "standardized"), showWarnings = FALSE)
    write.table(temps,file=file.path(file.path(getwd(), "standardized"),var),row.names=F)
    
  } else if (!bin & !std & cdl) {
    ### Conditional lookup table
    df[,var]<-as.character(df[,var])
    df[is.na(df[,var]),var]<-" MISSING"
    cm[,paste(var,".cdl",sep="")]<<-as.factor(df[,var]) 
    
    templ<-data.frame(as.numeric(dfo[,var]),df[,var])
    templ<-templ[order(templ[,1],decreasing=T),]
    lookup<-as.data.frame(templ[!duplicated(templ[,2]),])
    lookup<-lookup[order(lookup[,1]),]
    dimnames(lookup)[[2]]<-c("upper","range")
    dir.create(file.path(getwd(), "conditionals"), showWarnings = FALSE)
    write.table(lookup,file=file.path(file.path(getwd(), "conditionals"),var),row.names=F)
    
  }
}


### Create function for payer type analysis

analyze.payer<-function(df=pts,group="PayerType"){
  
  sum_charge<-aggregate(df$Charge,list(df[,group]),sum)
  sum_cost<-aggregate(df$Cost,list(df[,group]),sum)
  sum_payment<-aggregate(df$Payment,list(df[,group]),sum)
  ave_charge<-aggregate(df$Charge,list(df[,group]),mean)
  ave_cost<-aggregate(df$Cost,list(df[,group]),mean)
  ave_payment<-aggregate(df$Payment,list(df[,group]),mean)
  
  nobs<-table(df[,group],useNA="ifany")
  
  output<-data.frame(Patients=as.numeric(nobs),Payment_Over_Cost=round(sum_payment[,2]/sum_cost[,2],3),
                     Payment_Over_Charge=round(sum_payment[,2]/sum_charge[,2],3),
                     Ave_Charge=round(ave_charge[,2],2), Ave_Cost=round(ave_cost[,2],2),
                     Ave_Payment=round(ave_payment[,2],2),
                     row.names=names(nobs))
  
  Payer_Analysis<-output[order(output$Payment_Over_Charge,decreasing=T),]
  return(Payer_Analysis)
}

### Find mode of a set 
Mode <- function(x) {
  ux <- unique(x)
  index<-which(tabulate(match(x, ux))==max(tabulate(match(x, ux))))
  if (length(index)>1) index<-index[sample.int(length(index),1)]
  return(ux[index])
}

## Check Memory Usage

mem <- function() {
  bit <- 8L * .Machine$sizeof.pointer
  if (!(bit == 32L || bit == 64L)) {
    stop("Unknown architecture", call. = FALSE)
  }
  
  node_size <- if (bit == 32L) 28L else 56L
  
  usage <- gc()
  sum(usage[, 1] * c(node_size, 8)) / (1024 ^ 2)
}

### Factor to numeric

fac2num<-function(f) as.numeric(levels(f))[f]
fac2int<-function(f) as.integer(levels(f))[f]


### Distance formula -- used for parallel calculation

gcd.hf <- function(long1, lat1, long2, lat2) {
  R <- 3963.35 # Earth mean radius [miles]
  deg2rad <- function(deg) return(deg*pi/180)
  rlong1<-deg2rad(long1); rlong2<-deg2rad(long2); rlat1<-deg2rad(lat1); rlat2<-deg2rad(lat2)
  delta.long <- (rlong2 - rlong1)
  delta.lat <- (rlat2 - rlat1)
  a <- sin(delta.lat/2)^2 + cos(rlat1) * cos(rlat2) * sin(delta.long/2)^2
  c <- 2 * asin(sqrt(a))
  d = R * c
  return(d) 
}


### Function for geocoding addresses

library(RCurl)
library(RJSONIO)

construct.geocode.url <- function(address, return.call = "json", sensor = "false") {
  root <- "http://maps.google.com/maps/api/geocode/"
  u <- paste(root, return.call, "?address=", address, "&sensor=", sensor, sep = "")
  return(URLencode(u))
}

gGeoCode <- function(address,verbose=FALSE) {
  require("plyr")
  if(verbose) cat(address,"\n")
  u <- aaply(address,1,construct.geocode.url)
  doc <- aaply(u,1,getURL)
  json <- alply(doc,1,fromJSON,simplify = FALSE)
  coord = laply(json,function(x) {
    if(x$status=="OK") {
      lat <- x$results[[1]]$geometry$location$lat
      lng <- x$results[[1]]$geometry$location$lng
      return(c(lat, lng))
    } else {
      return(c(NA,NA))
    }
  })
  if(length(address)>1) colnames(coord)=c("lat","lng")
  else names(coord)=c("lat","lng")
  return(data.frame(address,coord))
}


#### Tools to combine IRM client databases

### Eval function for text

feval<-function(code)   eval(parse(text = code), envir = parent.frame())

### Table with missings

Table<-function(x) table(x,useNA="ifany")

### Function to change IDs 
### Soon will use IrmDB.dbo.HealthSystem instead of IRM_System.dbo.Tenant
library(RODBC)
sql<-odbcConnect("IrmSqls")
tenant<-sqlQuery(sql,paste0("SELECT HealthSystemID as TenantID, Name as ClientName, DatabaseName
                            from IRMDB.dbo.HealthSystem"),errors=T,stringsAsFactors = F,as.is=T)



chgid<-function(TenantID=78,dsname){
  dsname$PersonID_orig<-dsname$PersonID
  dsname$PersonID<-as.integer(paste0(TenantID,dsname$PersonID))
#  dsname$TenantID<-TenantID
#  dsname$ClientName<-tenant[tenant$TenantID==TenantID,"ClientName"]
  
#   if ("AddressID" %in% names(dsname)){
#     dsname$AddressID_orig<-dsname$AddressID
#     dsname$AddressID<-as.integer(paste0(TenantID,dsname$AddressID))
#   }
  return(dsname)
}

### Create function to perform select, apply chgid function
source("functions_cmty.R")

pts_select<-function(tenantid){
  
  dbname<-tenant[tenant$TenantID==tenantid,"DatabaseName"]
  
  sql<-odbcConnect("IrmSqls")
  
  pts_temp<-as.data.frame(sqlQuery(sql,
                                   paste0("SELECT 
                                          pe.EncounterID,
                                          pe.EncounterNumber,
                                          pe.PatientID,
                                          pe.EncounterAge,
                                          pe.Gender as EncounterGender,
                                          pe.Income,
                                          pe.AdmitDT as admitdate,
                                          pe.DischargeDT as dischargedate,
                                          pe.PresentOnAdmission,
                                          pe.Readmission,
                                          pat.PersonID,
                                          pat.PatientNumber,
                                          pm.IndividualId as individualid,
                                          pm.FamilyID as familyid,
                                          case 
                                          when pm.IndividualId is null OR pm.IndividualId='' then 0
                                          ELSE 1  
                                          END as ComPerFlag,
                                          case 
                                          when pm.FamilyID is null OR pm.FamilyID='' then 0
                                          ELSE 1  
                                          END as ComHHFlag,
                                          adr.AddressHash as AddressHashEncounter, adr.ZipCode as zipcodeencounter,
                                          adr.Longitude as long_enc, adr.Latitude as lat_enc,
                                          adc.Code as AdmitDiagFullCode,
                                          adc.Description as AdmitDiagDesc,
                                          adc.DisplayText as AdmitDiagDisplayText,
                                          adct.Description as AdmitDiagCodeType,
                                          adcg.Description as AdmitDiagCodeGroup, -- DIAGOSISCodeGroupID will need fixing
                                          ddc.Code as DischargeDiagFullCode,
                                          ddc.Description as DischargeDiagDesc,
                                          ddc.DisplayText as DischargeDiagDisplayText,
                                          ddct.Description as DischargeDiagCodeType,
                                          ddcg.Description as DischargeDiagCodeGroup,
                                          at.Description as AdmitType,
                                          tc.Description as TriageCode,
                                          co.Name as ClientOrganization,
                                          et.Description as EncounterType,
                                          pc.ProcedureCode as ProcedureFullCode,
                                          pc.Name as ProcedureShortDesc,
                                          pc.Description as ProcedureLongDesc,
                                          pc.ProcedureDisplayText as ProcedureDisplayText,
                                          pt.Description as ProcedureType,
                                          pg.Description as ProcedureGroup,
                                          ep.EncounterPaymentID, -- Might be more than one payer, dedup this by encID
                                          ep.Charge, 
                                          ep.Payment,
                                          ep.Cost,
                                          ins.Name as Payer,
                                          pay.Description as PayerType 
                      
                                          FROM ", dbname, ".HIPAA.Encounter pe
                                          left join ", dbname, ".HIPAA.Patient pat on pat.PatientID = pe.PatientID
                                          left join ", dbname, ".HIPAA.Person pers on pers.PersonID = pat.PersonID
                                          left join ", dbname, ".HIPAA.Address adr on adr.AddressID = pers.AddressID
                                          left join ", dbname, ".Community.PersonMatch pm on pat.PersonID = pm.PersonID
                                          left join ", dbname, ".Lookup.DiagnosisCode adc on adc.DiagnosisCodeID = pe.AdmitDiagnosisCodeID
                                          left Join ", dbname, ".Lookup.DiagnosisCodeType adct on adct.DiagnosisCodeTypeID = adc.DiagnosisCodeTypeID
                                          left Join ", dbname, ".Lookup.DiagnosisCodeGroup adcg on adcg.DiagosisCodeGroupID = adc.DiagnosisCodeGroupID
                                          left join ", dbname, ".Lookup.DiagnosisCode ddc on ddc.DiagnosisCodeID = pe.DischargeDiagnosisCodeID
                                          left Join ", dbname, ".Lookup.DiagnosisCodeType ddct on ddct.DiagnosisCodeTypeID = adc.DiagnosisCodeTypeID
                                          left Join ", dbname, ".Lookup.DiagnosisCodeGroup ddcg on ddcg.DiagosisCodeGroupID = adc.DiagnosisCodeGroupID
                                          left Join ", dbname, ".Lookup.AdmitType at on at.AdmitTypeID = pe.AdmitTypeID
                                          left join ", dbname, ".Lookup.TriageCode tc on tc.TriageCodeID = pe.TriageCodeID
                                          left join ", dbname, ".Configuration.ClientOrganization co on co.ClientOrganizationID = pe.ClientOrganizationID
                                          left join ", dbname, ".Lookup.EncounterType et on et.EncounterTypeID = pe.EncounterTypeID
                                          left join ", dbname, ".Lookup.ProcedureCode pc on pc.ProcedureCodeID = pe.ProcedureCodeID
                                          left join ", dbname, ".Lookup.ProcedureType pt on pt.ProcedureTypeID = pc.ProcedureTypeID
                                          left join ", dbname, ".Lookup.ProcedureGroup pg on pg.ProcedureGroupID = pc.ProcedureGroupID
                                          left join ", dbname, ".HIPAA.EncounterPayment ep on ep.EncounterID = pe.EncounterID
                                          left join ", dbname, ".HIPAA.Insurance ins on ins.InsuranceID = ep.InsuranceID
                                          left join ", dbname, ".Lookup.PayerType pay on pay.PayerTypeID = ins.PayerTypeID")
                                   ,errors=T,stringsAsFactors = F,as.is=18))
  pts_temp<-chgid(tenantid,pts_temp)
  pts_temp$ClientName_pt<-tenant[tenant$TenantID==tenantid,"ClientName"]
  return(pts_temp)
}

### Now create function to calls all the client dbs you want and concatenate them
## include timing
## create two functions, one for cmty, one for pts

## Cmty function

cmty_gather<-function(gtenids){
  print(paste0("Start at ",format(Sys.time(), "%b %d %X")))
  temp_out<-NULL
  chknames<-NULL
  for(i in 1:length(gtenids)){
    temp_c<-cmty_select(gtenids[i])
    
    ## Check names/dimensions match up
    if (i>1 && chknames!=names(temp_c)) {
      print(paste0("Names don't match for ",
                   tenant[tenant$TenantID==gtenids[i],"ClientName"]))
      break      
    }
    
    chknames<-names(temp_c)
    
    temp_out<-rbind(temp_out,temp_c)
    
    print(paste0(tenant[tenant$TenantID==gtenids[i],"ClientName"],
                 " finished at ",format(Sys.time(), "%b %d %X")))
  }
  return(temp_out)
}

### Pts Function

pts_gather<-function(gtenids){
  print(paste0("Start at ",format(Sys.time(), "%b %d %X")))
  temp_out<-NULL
  chknames<-NULL
  for(i in 1:length(gtenids)){
    temp_c<-pts_select(gtenids[i])
    
    ## Check names/dimensions match up
    if (i>1 && chknames!=names(temp_c)) {
      print(paste0("Names don't match for ",
                   tenant[tenant$TenantID==gtenids[i],"ClientName"]))
      break      
    }
    
    chknames<-names(temp_c)
    
    temp_out<-rbind(temp_out,temp_c)
    
    print(paste0(tenant[tenant$TenantID==gtenids[i],"ClientName"],
                 " finished at ",format(Sys.time(), "%b %d %X")))
  }
  return(temp_out)
}

### Get zips for all dbs


get_zips<-function(gtenids){
  temp_out<-NULL
  for(i in 1:length(gtenids)){
    sql<-odbcConnect("IrmSqls")
    temp_c<-sqlQuery(sql,paste0("SELECT distinct sa.Zipcode, co.Name as ClientOrganization, ",
                                "\'",gsub("\'","\'\'",tenant[tenant$TenantID==gtenids[i],"ClientName"]),"\'", " as ClientName",
                                " from ", tenant[tenant$TenantID==gtenids[i],"DatabaseName"], ".Configuration.StarkArea sa ",
                                "left join ", tenant[tenant$TenantID==gtenids[i],"DatabaseName"], ".Configuration.ClientOrganization co",
                                " on co.ClientOrganizationID = sa.ClientOrganizationID"),
                     errors=T,stringsAsFactors = F,as.is=T)
    temp_out<-rbind(temp_out,temp_c)
  }
  return(temp_out)
}

######### Dups within groups

dupsBetweenGroups <- function (df, idcol) {
  # df: the data frame
  # idcol: the column which identifies the group each row belongs to
  # Get the data columns to use for finding matches
  datacols <- setdiff(names(df), idcol)
  # Sort by idcol, then datacols. Save order so we can undo the sorting later.
  sortorder <- do.call(order, df)
  df <- df[sortorder,]
  # Find duplicates within each id group (first copy not marked)
  dupWithin <- duplicated(df)
  # With duplicates within each group filtered out, find duplicates between groups. 
  # Need to scan up and down with duplicated() because first copy is not marked.
  dupBetween = rep(NA, nrow(df))
  dupBetween[!dupWithin] <- duplicated(df[!dupWithin,datacols])
  dupBetween[!dupWithin] <- duplicated(df[!dupWithin,datacols], fromLast=TRUE) | dupBetween[!dupWithin]
  # =================== Replace NA's with previous non-NA value =====================
  # This is why we sorted earlier - it was necessary to do this part efficiently
  
  # Get indexes of non-NA's
  goodIdx <- !is.na(dupBetween)
  # These are the non-NA values from x only
  # Add a leading NA for later use when we index into this vector
  goodVals <- c(NA, dupBetween[goodIdx])
  
  # Fill the indices of the output vector with the indices pulled from
  # these offsets of goodVals. Add 1 to avoid indexing to zero.
  fillIdx <- cumsum(goodIdx)+1
  
  # The original vector, now with gaps filled
  dupBetween <- goodVals[fillIdx]
  
  # Undo the original sort
  dupBetween[sortorder] <- dupBetween
  
  # Return the vector of which entries are duplicated across groups
  return(dupBetween)
}
